Practicas de computación paralela
=================================

Para ejecutar los ejemplos:
---------------------------

Todas las salidas son en formato *.csv para una mejor importación en excel.

- **MPI**:
	- Producto Matriz-Vector:	
	  - De 1 a 4 procesos con N = 12000:   ./practicasMPI/matrizVector/compileAndRunA.sh
	  - De 1 y 4 procesos con N variable:  ./practicasMPI/matrizVector/compileAndRunB.sh
	- Producto Matriz-Matiz:
	  - De 1 a 4 procesos con N = 1200:    ./practicasMPI/matrizMatriz/compileAndRunA.sh
	  - De 1 y 4 procesos con N variable:  ./practicasMPI/matrizMatriz/compileAndRunB.sh

- **OpenMP**:
  - Producto Matriz-Vector:	
	  - De 1 a 4 procesos con N = 12000:   ./practicasOpenMP/matrizVector/compileAndRun.sh
	- Producto Matriz-Matiz:
	  - De 1 a 4 procesos con N = 1200:    ./practicasOpenMP/matrizMatriz/compileAndRun.sh


- **CUDA**:
	- Suma de matrices
	  - No se han implementado scripts
	- Matriz por vector
	  - No se han implementado scripts


Documentación:
--------------

- Ejemplo web para la generación de la multiplicación.
	*http://es.easycalculation.com/matrix/learn-matrix-multiplication.php*
	*http://matrix.reshish.com/multiplication.php*
