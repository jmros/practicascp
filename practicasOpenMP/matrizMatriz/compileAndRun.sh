# Archivo de generación de la practica
# parametro 1: número de procesos
# parametro 2: csv = 1
#
gcc -fopenmp -o pr2_b pr2_b.c
if [ -f ./pr2_b ]; then
echo "numero procesos;N;tiempo modo a filas adyacentes;tiempo modo a filas adyacentes por bloques de 10;tiempo modo a columnas adyacentes;tiempo modo a columnas adyacentes por bloques de 10; tiempo modo b"
./pr2_b 1 1
./pr2_b 2 1
./pr2_b 3 1
./pr2_b 4 1
rm -rf pr2_b
fi

