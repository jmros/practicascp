/* 
 * File:   pr2_a.c
 * Author: jmros
 *
 * Created on 26 de diciembre de 2013, 21:02
 */

#include <stdio.h>
#include <string.h> /* memset  necesario para que funcione memset*/
#include <unistd.h> /* close necesario para que funcione memset*/
#include <stdlib.h>
#include <omp.h>
#include <locale.h>

int N = 1200;
int muestraInfo = 0;
int csv = 0;

int main(int argc, char *argv[]) {
    setlocale(LC_ALL,"es_ES.UTF-8");

    int nproces = 4, iam, i, j, k;
    double **matrizA, **matrizB, **matrizResultado;
    double suma;
    int tamBloque, inicioBloque, finBloque;
    double tStart, tEnd;

    srand(time(NULL));
    srand48(time(NULL));

    int nprocesAux = atoi(argv[1]);
    if (nprocesAux) {
        nproces = nprocesAux;
    }

    if (argv[2] && (atoi(argv[2]) == 1)) {
        csv = 1;
        printf("%d;%d;", nproces, N);
    }


    matrizA = (double**) malloc(sizeof *matrizA * N);
    matrizB = (double**) malloc(sizeof *matrizB * N);
    matrizResultado = (double**) malloc(sizeof *matrizResultado * N);
    for (i = 0; i < N; i++) {
        matrizA[i] = (double*) malloc(sizeof *matrizA[i] * N);
        matrizB[i] = (double*) malloc(sizeof *matrizB[i] * N);
        matrizResultado[i] = (double*) malloc(sizeof *matrizResultado[i] * N);
    }
    //rellenamos los elementos
    for (i = 0; i < N; i++) {
        for (j = 0; j < N; j++) {
            matrizA[i][j] = (double) drand48();
            matrizB[i][j] = (double) drand48();
        }
    }

    //    printf("MatrizA\n");
    //    for (i = 0; i < N; i++) {
    //        for (j = 0; j < N; j++) {
    //            printf(" %.2f", matrizA[i][j]);
    //        }
    //        printf("\n");
    //    }
    //
    //    printf("MatrizB\n");
    //    for (i = 0; i < N; i++) {
    //        for (j = 0; j < N; j++) {
    //            printf(" %.2f", matrizA[i][j]);
    //        }
    //        printf("\n");
    //    }

    tStart = omp_get_wtime();
    //operaciones

#pragma omp parallel num_threads(nproces) private(iam,j,i,k, inicioBloque, finBloque,tamBloque, suma) default(shared)
    {
        iam = omp_get_thread_num();
#pragma omp single copyprivate(tamBloque)
        {
            tamBloque = N / nproces;
        }
        inicioBloque = tamBloque * iam;
        finBloque = tamBloque * (iam + 1); //si es un proceso al menos tiene que hacer una fila

        if (iam == (nproces - 1)) { //si soy el ultimo proceso
            finBloque = N;
        }

        for (i = inicioBloque; i < finBloque; i++) {
            for (j = 0; j < N; j++) {
                for (k = 0; k < N; k++) {
                    matrizResultado[i][j] += matrizA[i][k] * matrizB[k][j];
                }

            }
        }
    }
    tEnd = omp_get_wtime();

    //    printf("Matriz resultado A.1\n");
    //    for (i = 0; i < N; i++) {
    //        for (j = 0; j < N; j++) {
    //            printf(" %.2f", matrizResultado[i][j]);
    //        }
    //        printf("\n");
    //    }



    if (csv == 1) {
        printf("%.41f;", tEnd - tStart);
    } else {
        printf("Modo A filas adyacentes. Tiempo total en segundos: %.41f para %d procesos y N %d\n", tEnd - tStart, nproces, N);
    }

    for (i = 0; i < N; i++) {
        memset(matrizResultado[i], 0, N * sizeof (matrizResultado[0][0]));
    }

    tStart = omp_get_wtime();
#pragma omp parallel for schedule(static,10) num_threads(nproces) private(i,j,k) default(shared)
    for (i = 0; i < N; i++) {
        for (j = 0; j < N; j++) {
            for (k = 0; k < N; k++) {
                matrizResultado[i][j] += matrizA[i][k] * matrizB[k][j];
            }

        }
    }
    tEnd = omp_get_wtime();

    //    printf("Matriz resultado A.2\n");
    //    for (i = 0; i < N; i++) {
    //        for (j = 0; j < N; j++) {
    //            printf(" %.2f", matrizResultado[i][j]);
    //        }
    //        printf("\n");
    //    }



    if (csv == 1) {
        printf("%.41f;", tEnd - tStart);
    } else {
        printf("Modo A filas adyacentes (grupos de 10). Tiempo total en segundos: %.41f para %d procesos y N %d\n", tEnd - tStart, nproces, N);
    }

    for (i = 0; i < N; i++) {
        memset(matrizResultado[i], 0, N * sizeof (matrizResultado[0][0]));
    }

    tStart = omp_get_wtime();
    //operaciones

#pragma omp parallel num_threads(nproces) private(iam,j,i,k, inicioBloque, finBloque,tamBloque, suma) default(shared)
    {
        iam = omp_get_thread_num();
#pragma omp single copyprivate(tamBloque)
        {
            tamBloque = N / nproces;
        }
        inicioBloque = tamBloque * iam;
        finBloque = tamBloque * (iam + 1); //si es un proceso al menos tiene que hacer una fila

        if (iam == (nproces - 1)) { //si soy el ultimo proceso
            finBloque = N;
        }

        for (j = 0; j < N; j++) {
            for (i = inicioBloque; i < finBloque; i++) {
                for (k = 0; k < N; k++) {
                    matrizResultado[i][j] += matrizA[i][k] * matrizB[k][j];
                }

            }
        }
    }
    tEnd = omp_get_wtime();

    //    printf("Matriz resultado B.1\n");
    //    for (i = 0; i < N; i++) {
    //        for (j = 0; j < N; j++) {
    //            printf(" %.2f", matrizResultado[i][j]);
    //        }
    //        printf("\n");
    //    }



    if (csv == 1) {
        printf("%.41f;", tEnd - tStart);
    } else {
        printf("\nModo A columnas adyacentes. Tiempo total en segundos: %.41f para %d procesos y N %d\n", tEnd - tStart, nproces, N);
    }

    for (i = 0; i < N; i++) {
        memset(matrizResultado[i], 0, N * sizeof (matrizResultado[0][0]));
    }

    tStart = omp_get_wtime();
    for (i = 0; i < N; i++) {
        for (j = 0; j < N; j++) {
            suma = 0;
#pragma omp parallel for schedule(static,10) num_threads(nproces) private(k) default(shared) reduction (+:suma)
            for (k = 0; k < N; k++) {
                suma += matrizA[i][k] * matrizB[k][j];
            }
            matrizResultado[i][j] = suma;
        }
    }
    tEnd = omp_get_wtime();

    //    printf("Matriz resultado B.2\n");
    //    for (i = 0; i < N; i++) {
    //        for (j = 0; j < N; j++) {
    //            printf(" %.2f", matrizResultado[i][j]);
    //        }
    //        printf("\n");
    //    }


    if (csv == 1) {
        printf("%.41f;", tEnd - tStart);
    } else {
        printf("Modo A columnas adyacentes (grupos de 10). Tiempo total en segundos: %.41f para %d procesos y N %d\n", tEnd - tStart, nproces, N);
    }


    tStart = omp_get_wtime();


    for (j = 0; j < N; j++) {
        for (i = N; i < N; i++) {
            suma = 0;
#pragma omp parallel for reduction(+:suma) num_threads(nproces) private(j) default(shared)
            for (k = 0; k < N; k++) {
                suma += matrizA[i][k] * matrizB[k][j];
            }
            matrizResultado[i][j] += suma;

        }
    }

    tEnd = omp_get_wtime();

    //    printf("Matriz resultado B.2\n");
    //    for (i = 0; i < N; i++) {
    //        for (j = 0; j < N; j++) {
    //            printf(" %.2f", matrizResultado[i][j]);
    //        }
    //        printf("\n");
    //    }

    if (csv == 1) {
        printf("%.41f\n", tEnd - tStart);
    } else {
        printf("Modo B. Tiempo total en segundos: %.41f para %d procesos y N %d\n", tEnd - tStart, nproces, N);
    }





    free(matrizA);
    free(matrizB);
    free(matrizResultado);

}

