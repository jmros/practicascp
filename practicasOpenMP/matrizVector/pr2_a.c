/* 
 * File:   pr2_a.c
 * Author: jmros
 *
 * Created on 26 de diciembre de 2013, 21:02
 */

#include <stdio.h>
#include <string.h> /* memset  necesario para que funcione memset*/
#include <unistd.h> /* close necesario para que funcione memset*/
#include <stdlib.h>
#include <omp.h>
#include <locale.h>

int N = 12000;
int muestraInfo = 0;
int csv = 0;

int main(int argc, char *argv[]) {
    setlocale(LC_ALL,"es_ES.UTF-8");

    int nproces = 4, iam, i, j;
    double **matriz;
    double *vector, *vectorResultado;
    double suma;
    int tamBloque, inicioBloque, finBloque;
    double tStart, tEnd;

    srand(time(NULL));
    srand48(time(NULL));

    int nprocesAux = atoi(argv[1]);
    if (nprocesAux) {
        nproces = nprocesAux;
    }

    if (argv[2] && (atoi(argv[2]) == 1)) {
        csv = 1;
        printf("%d;%d;", nproces, N);
    }

    matriz = (double**) malloc(sizeof *matriz * N);
    for (i = 0; i < N; i++) {
        matriz[i] = (double*) malloc(sizeof *matriz[i] * N);
    }
    vector = (double*) malloc(sizeof *vector * N);
    vectorResultado = (double*) malloc(sizeof *vectorResultado * N);

    //rellenamos los elementos
    for (i = 0; i < N; i++) {
        for (j = 0; j < N; j++) {
            matriz[i][j] = (double) drand48();
        }
    }

    for (i = 0; i < N; i++) {
        vector[i] = (double) drand48();
    }

    //    for (i = 0; i < N; i++) {
    //        for (j = 0; j < N; j++) {
    //            printf(" %.2f", matriz[i][j]);
    //        }
    //        printf("\n");
    //    }
    //
    //    for (i = 0; i < N; i++) {
    //        printf(" %.2f", vector[i]);
    //        printf("\n");
    //    }


    tStart = omp_get_wtime();
    //matriz y vector son compartirdas, por lo tanto puedo acceder a ellas
#pragma omp parallel num_threads(nproces) private(iam,j,i, inicioBloque, finBloque,tamBloque, suma) default(shared)
    {
        iam = omp_get_thread_num();
#pragma omp single copyprivate(tamBloque)
        {
            tamBloque = N / nproces;
        }
        inicioBloque = tamBloque * iam;
        finBloque = tamBloque * (iam + 1); //si es un proceso al menos tiene que hacer una fila

        if (iam == (nproces - 1)) { //si soy el ultimo proceso
            finBloque = N;
        }

        //operaciones
        for (i = inicioBloque; i < finBloque; i++) {
            suma = 0;
            for (j = 0; j < N; j++) {
                suma += matriz[i][j] * vector[j];
            }
            vectorResultado[i] += suma;
        }



    }
    
    tEnd = omp_get_wtime();
    
    //    printf("esto es el resultado\n");
    //    for (i = 0; i < N; i++) {
    //        printf(" %.2f\t", vectorResultado[i]);
    //    }
    //    printf("\n");

    if (csv == 1) {
        printf("%.41f;",tEnd - tStart);
    } else {
        printf("modo A. Tiempo total en segundos: %.41f para %d procesos y N %d\n", tEnd - tStart, nproces, N);
    }
    
    
    //    printf("esto es el resultado tras borrarlo\n");
    //    for (i = 0; i < N; i++) {
    //        printf(" %.2f\t", vectorResultado[i]);
    //    }
    //    printf("\n");

    memset(vectorResultado, 0, N * sizeof (vectorResultado[0]));
    
    tStart = omp_get_wtime();

    //operaciones
    for (i = 0; i < N; i++) {
        suma = 0;
#pragma omp parallel for reduction(+:suma) num_threads(nproces) private(j) default(shared)
        for (j = 0; j < N; j++) {
            suma += matriz[i][j] * vector[j];
        }
        vectorResultado[i] += suma;
    }

    //    printf("esto es el resultado b\n");
    //    for (i = 0; i < N; i++) {
    //        printf(" %.2f\t", vectorResultado[i]);
    //    }
    //    printf("\n");

    tEnd = omp_get_wtime();
    if (csv == 1) {
        printf("%.41f\n",tEnd - tStart);
    } else {
        printf("modo B. Tiempo total en segundos: %.41f para %d procesos y N %d\n", tEnd - tStart, nproces, N);
    }
    
    free(matriz);
    free(vector);
    free(vectorResultado);
    
}

