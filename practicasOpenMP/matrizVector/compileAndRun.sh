# Archivo de generación de la practica
# parametro 1: número de procesos
# parametro 2: csv = 1
#
gcc -fopenmp -o pr2_a pr2_a.c
if [ -f ./pr2_a ]; then
echo "numero procesos;N;tiempo modo a; tiempo modo b"
./pr2_a 1 1
./pr2_a 2 1
./pr2_a 3 1
./pr2_a 4 1
rm -rf pr2_a
fi
