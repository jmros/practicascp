
/* Includes, system */
#include <stdio.h>

#include <cuda.h>
#include <omp.h>
#include <cuda_runtime.h>
#define N_menor 256
#define N 256
#define BLOCK_SIZE 16
#define GRID_SIZE 16 // GRID_SIZE = N/BLOCK_SIZE

/* DEVICE CODE */
__device__  int suma_dos_enteros(int a, int b){
		return (a+b);
}

__global__ void suma_2_vectores(int *d1, int *d2, int *sum){

	
        int idBloque = blockIdx.y * gridDim.x + blockIdx.x;
        int idThread = idBloque * blockDim.z * blockDim.y * blockDim.x + threadIdx.z * blockDim.y * blockDim.x + threadIdx.y * blockDim.x + threadIdx.x;
		if (idThread < N_menor){
				sum[idThread] = suma_dos_enteros(d1[idThread], d2[idThread]);
				//sum[idThread] = d1[idThread] + d2[idThread];
			}
}

/* HOST CODE*/
int main(int argc, char** argv)
{
    int DeviceCount = 0,i,j, csv;
	int **h_matrizA, **h_matrizB, **h_matrizResultado;
	int *d_d1,*d_d2,*d_sum;
	double tStart, tEnd, tTotal;
	
	srand(time(NULL));
    srand48(time(NULL));
	
	if (argv[1] && (atoi(argv[1]) == 1)) {
        csv = 1;
    }
	
	
	//RESERVA MEMORIA CPU (HOST)
	h_matrizA = (int**) malloc(N*sizeof(int*));
	h_matrizB = (int**) malloc(N*sizeof(int*));
	h_matrizResultado = (int**) malloc(N*sizeof(int*));
    for (i = 0; i < N; i++) {
        h_matrizA[i] = (int*) malloc(sizeof(int) * N);
		h_matrizB[i] = (int*) malloc(sizeof(int) * N);
		h_matrizResultado[i] = (int*) malloc(sizeof(int) * N);
    }
	for (i = 0; i < N; i++) {
        for (j = 0; j < N; j++) {
            h_matrizA[i][j] = (i+j)+1;
			h_matrizB[i][j] = (i+j)+1;
        }
    }
	/* printf(" Matriz A\n");
	    for (i = 0; i < N; i++) {
           for (j = 0; j < N; j++) {
                printf(" %d\t", h_matrizA[i][j]);
            }
            printf("\n");
        }
		
		printf(" Matriz B\n");
		for (i = 0; i < N; i++) {
           for (j = 0; j < N; j++) {
                printf(" %d\t", h_matrizB[i][j]);
            }
            printf("\n");
        }
	
    */

	 if (csv == 1) {
		printf("%d;%d;%d;", N, BLOCK_SIZE,GRID_SIZE);
    } else {
        printf("N: %d;\t BLOCK_SIZE:%d;\t GRID_SIZE:%d;\t", N, BLOCK_SIZE,GRID_SIZE);
    }


	//INICIALIZACION CUDA
   
    /* Initialize CUDA */
    if (cuInit(0) != 0){
        printf("ERROR de inicializacion\n");
        exit(0);
    }
//OBTENCION DE GPUs EN EL SISTEMA        
    cuDeviceGetCount(&DeviceCount);
    if (DeviceCount == 0){
        printf("ERROR ningun dispositivo soporta CUDA\n");
        exit(0);
    }

	//RESERVA DE MEMORIA EN LA GPU (DEVICE)       
    cudaMalloc((void**)&d_d1,N*sizeof(d_d1));cudaMemset(d_d1,0,N*sizeof(d_d1));
    cudaMalloc((void**)&d_d2,N*sizeof(d_d2));cudaMemset(d_d2,0,N*sizeof(d_d2));
    cudaMalloc((void**)&d_sum,N*sizeof(d_sum));cudaMemset(d_sum,0,N*sizeof(d_sum));

	//COPIA DE MEMORIA DEL HOST (CPU) AL DEVICE (GPU)

	for (i = 0; i < N; i++) {
		
		cudaMemcpy(d_d1,h_matrizA[i],N*sizeof(h_matrizA[i][0]),cudaMemcpyHostToDevice);
		cudaMemcpy(d_d2,h_matrizB[i],N*sizeof(h_matrizB[i][0]),cudaMemcpyHostToDevice);
		
		
		//SUMA VECTORES
		//LOS VECTORES YA ESTAN EN MEMORIA DE GPU NO HACE FALTA COPIARLOS DE NUEVO
		tStart = omp_get_wtime();
		
		suma_2_vectores<<<GRID_SIZE,BLOCK_SIZE>>>(d_d1,d_d2,d_sum);
		cudaThreadSynchronize();
		
		tEnd = omp_get_wtime();
		tTotal += tEnd-tStart;
		
		//COPIA DE MEMORIA DEL DEVICE (GPU) AL HOST(CPU)
		cudaMemcpy(h_matrizResultado[i],d_sum,N*sizeof(h_matrizResultado[i][0]),cudaMemcpyDeviceToHost); //---JOSE>N Y NO 8000 PORQUE ME INTERESA PARA LOS PRINTEOS EN LA LIENEA 143
	}

	/* printf(" Matriz Resultado\n");
	    for (i = 0; i < N; i++) {
           for (j = 0; j < N; j++) {
                printf(" %d\t", h_matrizResultado[i][j]);
            }
            printf("\n");
        }
	printf("\n"); */
	
	if (csv == 1) {
		printf("%.41f;", tTotal);
    } else {
        printf("Tiempo Total GPU:%.41f\t", tTotal);
    }
	
	
	
	//suma utilizando la CPU
	for (i = 0; i < N; i++) {
        for (j = 0; j < N; j++) {
            h_matrizResultado[i][j]= 0;
        }
    }
	
	
	tStart = omp_get_wtime();
	for (i = 0; i < N; i++) {
        for (j = 0; j < N; j++) {
            h_matrizResultado[i][j]= h_matrizA[i][j]+h_matrizB[i][j];
        }
    }
    tEnd = omp_get_wtime();
	tTotal += tEnd-tStart;
	
	if (csv == 1) {
		printf("%.41f\n", tTotal);
    } else {
        printf("Tiempo Total CPU:%.41f\n", tTotal);
    }
	
    
     
    cudaFree(d_d1);cudaFree(d_d2);cudaFree(d_sum);
    free(h_matrizA);free(h_matrizB);free(h_matrizResultado);
}
