#rm *.o
#rm *.out
#cp nlcg_cuda.c nlcg_cuda.cu
#gcc -fopenmp -o pr2_b pr2_b.c
if [ -f ./pr3_b.cu ]; then
nvcc -Xcompiler -fopenmp -use_fast_math  -arch sm_13 -lcuda  -o pr3_b pr3_b.cu
fi
if [ -f ./pr3_b ]; then
echo "N;BLOCK_SIZE;GRID_SIZE;tTotal"
./pr3_b 1
rm pr3_b
fi

