
/* Includes, system */
#include <stdio.h>
#include <locale.h>

#include <cuda.h>
#include <cuda_runtime.h>
#include <omp.h>


#define N 512
#define BLOCK_SIZE 512
#define GRID_SIZE 1 // GRID_SIZE = N/BLOCK_SIZE

/* DEVICE CODE */
__global__ void IntProd(int* intprodParcial,int* d1,int* d2){

    __shared__ double accumResult[BLOCK_SIZE];

    int pos = blockIdx.x * blockDim.x + threadIdx.x;
    int iAccum = threadIdx.x;	
    int vec =  blockIdx.x;

    accumResult[iAccum] = d1[pos] * d2[pos];
//    accumResult[iAccum] = d1[pos];

    for(int stride = BLOCK_SIZE / 2; stride > 0; stride >>= 1){
        __syncthreads();
        accumResult[iAccum] += accumResult[stride + iAccum];
    }

    if(threadIdx.x == 0) {
        intprodParcial[vec] = accumResult[0];
    }
}



/* HOST CODE*/
int main(int argc, char** argv){
	setlocale(LC_ALL,"es_ES.UTF-8");
		
	int csv, num_err=0;
    int DeviceCount = 0,i,j,k;
    int **h_matriz; 
	int *h_matrizToVector, *h_vector, *h_vectorResultadoParcial, *h_vectorResultadoFinal;
	int *d_matrizToVector,*d_vector,*d_vectorResultadoParcial;
    int idDevice=0;
    cudaDeviceProp deviceProp;
	double tStart, tEnd, tTotal;
	
	srand(time(NULL));
    srand48(time(NULL));
	if (argv[1] && (atoi(argv[1]))) {
        csv = 1;
    }


//DATOS DE LA GRAFICA PARTICULAR QUE USAMOS

    cudaGetDeviceProperties(&deviceProp, idDevice);
	 
	if (csv != 1){
	 
		printf("\nDispositivo CUDA  %d: \"%s\"\n", idDevice, deviceProp.name);
		printf("  Cantidad TOTAL de memoria GLOBAL:  %.0f MBytes (%llu bytes)\n", (float)deviceProp.totalGlobalMem/1048576.0f, (unsigned long long) deviceProp.totalGlobalMem);
		printf("  Cantidad SHARED por BLOQUE:  %.0f KBytes (%llu bytes)\n", (float)deviceProp.sharedMemPerBlock/1024.0f, (unsigned long long) deviceProp.sharedMemPerBlock);
		printf("  Tamano Warp: %d\n", deviceProp.warpSize); //---JOSE>POR DEFECTO SIEMPRE HA SIDO 16
		printf("  Tamano maximo de bloque (numero de hilos maximo por bloque):  %d\n", deviceProp.maxThreadsPerBlock);
		printf("  Dimensiones maximas de bloque:    %d x %d x %d\n",
		   deviceProp.maxThreadsDim[0],
		   deviceProp.maxThreadsDim[1],
		   deviceProp.maxThreadsDim[2]);
		printf("  Dimensiones maximas de grid:     %d x %d x %d\n",
		   deviceProp.maxGridSize[0],
		   deviceProp.maxGridSize[1],
		   deviceProp.maxGridSize[2]);
		printf("  Capacidad computacion (sm): %d.%d.\n", deviceProp.major,deviceProp.minor);
	}
			 

//RESERVA MEMORIA CPU (HOST)
	h_matriz = (int**) malloc(N*sizeof(int*));
	for (i = 0; i < N; i++) {
       h_matriz[i] = (int*) malloc(sizeof(int) * N);
	}
	h_matrizToVector = (int*) malloc (sizeof(int) * N * N);
	h_vector = (int*) malloc(sizeof(int) * N);
	h_vectorResultadoParcial = (int*) malloc(sizeof(int) * N);
	h_vectorResultadoFinal = (int*) malloc(sizeof(int) * N);
	k=0;
	for (i = 0; i < N; i++) {
        for (j = 0; j < N; j++) {
            h_matriz[i][j] = k++;
		}
	}
	for (j = 0; j < N; j++) {
        h_vector[j] = (j)+1;
		h_vectorResultadoParcial[j]=0;
		h_vectorResultadoFinal[j]=0;
	}
	// printf(" Matriz\n");
	    // for (i = 0; i < N; i++) {
           // for (j = 0; j < N; j++) {
                // printf(" %d\t", h_matriz[i][j]);
            // }
            // printf("\n");
        // }
		
		// printf(" Vector\n");
		// for (j = 0; j < N; j++) {
                // printf(" %d\t", h_matriz[j]);
            // }
            // printf("\n");
		
	//convertimos la matriz a un vector para que quede la memoria contigua y buscar equalescencia
	 
	k=0;
	for (i = 0; i < N; i++) {
        for (j = 0; j < N; j++) {
            h_matrizToVector[k] = h_matriz[i][j];
			k++;
        }
    }
	


	//INICIALIZACION CUDA
    if (cuInit(0) != 0){ //---JOSE>TENGO CUDA INSTALADO
        printf("ERROR de inicializacion\n");
        exit(0);
    }
	//OBTENCION DE GPUs EN EL SISTEMA        
    cuDeviceGetCount(&DeviceCount);
    if (DeviceCount == 0){
        printf("ERROR ningun dispositivo soporta CUDA\n");
        exit(0);
    }

	//RESERVA DE MEMORIA EN LA GPU (DEVICE)
    if (cudaMalloc((void**)&d_matrizToVector,N*N*sizeof(d_matrizToVector)) != 0){
		num_err++;
	}	
	if (cudaMalloc((void**)&d_vector,N*sizeof(d_vector)) != 0){
		num_err++;
	}
    if (cudaMalloc((void**)&d_vectorResultadoParcial,N*sizeof(d_vectorResultadoParcial)) != 0){
		num_err++;
	}
	if (num_err != 0) {
        printf ("ERROR reserva memoria device: %d\n",num_err);
        return EXIT_FAILURE;
    }

	cudaMemset(d_matrizToVector,0,N*N*sizeof(d_matrizToVector));
	cudaMemset(d_vector,0,N*sizeof(d_vector));
	cudaMemset(d_vectorResultadoParcial,0,N*sizeof(d_vectorResultadoParcial));
	
	//COPIA DE MEMORIA DEL HOST (CPU) AL DEVICE (GPU)
    if(cudaMemcpy(d_matrizToVector,h_matrizToVector,N*N*sizeof(h_matrizToVector[0]),cudaMemcpyHostToDevice) != 0){
		printf("Error cudaMemcpy matrizToVector");
	}
	if (cudaMemcpy(d_vector,h_vector,N*sizeof(h_vector[0]),cudaMemcpyHostToDevice) != 0){
		printf("Error cudaMemcpy vector");
	}
 
	//Inicio de las operaciones
	for (i=0;i<N;i++) {
		tStart = omp_get_wtime();
	
		//LLAMADA AL KERNEL DEL PRODUCTO INTERNO
		IntProd<<<GRID_SIZE,BLOCK_SIZE>>>(d_vectorResultadoParcial,&d_matrizToVector[i*N],d_vector);
		cudaThreadSynchronize();
		 
		tEnd = omp_get_wtime();
		tTotal += tEnd - tStart;

		//COPIA DE MEMORIA DEL DEVICE (GPU) AL HOST(CPU)
		//recuperación de los resultados parciales
		cudaMemcpy(h_vectorResultadoParcial,d_vectorResultadoParcial,GRID_SIZE*sizeof(h_vectorResultadoParcial[0]),cudaMemcpyDeviceToHost);
		//FINALIZACION DEL PRODUCTOINTERNO EN EL HOST (CPU)
		for (j=0;j<GRID_SIZE;j++){
			h_vectorResultadoFinal[j] += h_vectorResultadoParcial[j];
		}
		 
	 
		
	}

	if (csv == 1){
		printf("%d;%d;%d;%.41f\n", N,BLOCK_SIZE,GRID_SIZE,tTotal);
	}else{
		printf("\nN = %d, BLOCK_SIZE = %d, GRID_SIZE = %d, Tiempo Total = %.9lf segundos\n",N,BLOCK_SIZE,GRID_SIZE,tTotal);
	}
	
    
     
    cudaFree(d_matrizToVector);cudaFree(d_vector);cudaFree(d_vectorResultadoParcial);
	free(h_matriz);
	free(h_matrizToVector);free(h_vector);free(h_vectorResultadoParcial);free(h_vectorResultadoFinal);
}
