# Archivo de generación de la practica
# parametro 1: N
# parametro 2: csv = 1
#
mpicc -o pr1 pr1_b.c
if [ -f ./pr1 ]; then
echo 'numero procesos;N;tiempo'
mpirun -np 1 pr1 1200 1
mpirun -np 2 pr1 1200 1
mpirun -np 3 pr1 1200 1
mpirun -np 4 pr1 1200 1
rm pr1
rm pr1_b.o
fi


