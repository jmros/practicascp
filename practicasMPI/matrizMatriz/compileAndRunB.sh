# Archivo de generación de la practica
# parametro 1: N
# parametro 2: csv = 1
#
mpicc -o pr1 pr1_b.c
if [ -f ./pr1 ]; then
echo 'numero procesos;N;tiempo'
mpirun -np 1 pr1 200 1
mpirun -np 1 pr1 400 1
mpirun -np 1 pr1 600 1
mpirun -np 1 pr1 800 1
mpirun -np 1 pr1 1000 1
mpirun -np 1 pr1 1200 1
mpirun -np 1 pr1 1400 1
mpirun -np 1 pr1 1600 1
mpirun -np 1 pr1 1800 1
mpirun -np 1 pr1 2000 1
#mpirun -np 1 pr1 16000 1
#mpirun -np 1 pr1 18000 1
#mpirun -np 1 pr1 20000 1
mpirun -np 4 pr1 200 1
mpirun -np 4 pr1 400 1
mpirun -np 4 pr1 600 1
mpirun -np 4 pr1 800 1
mpirun -np 4 pr1 1000 1
mpirun -np 4 pr1 1200 1
mpirun -np 4 pr1 1400 1
mpirun -np 4 pr1 1600 1
mpirun -np 4 pr1 1800 1
mpirun -np 4 pr1 2000 1
#mpirun -np 4 pr1 16000 1
#mpirun -np 4 pr1 18000 1
#mpirun -np 4 pr1 20000 1
rm pr1
rm pr1_b.o
fi


