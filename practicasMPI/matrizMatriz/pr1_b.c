#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <locale.h>


//mpicc -o pr1 pr1.c && mpirun -np 4 pr1 -nprocess=5


int N = 2000;
int muestraInfo = 0;
int csv = 0;
//Programa valido para N divisible entero por np

int main(int argc, char *argv[]) {
    setlocale(LC_ALL,"es_ES.UTF-8");


    int nproces, myrank, i, j, k;
    double **matrizA, **matrizB, **matrizResultado, *vectorTemp;
    int tamBloque;
    double tStart, tEnd;

    srand(time(NULL));
    srand48(time(NULL));

    MPI_Status status;

    MPI_Init(&argc, &argv);

    int nAux = atoi(argv[1]);
    if (nAux) {
        N = nAux;
    }


    if (argv[2] && (atoi(argv[2]) == 1)) {
        csv = 1;
    }

    MPI_Comm_size(MPI_COMM_WORLD, &nproces);
    MPI_Comm_rank(MPI_COMM_WORLD, &myrank);

    if (muestraInfo == 1) {
        printf("Soy el proceso %d... con N = %d \n", myrank, N);
    }

    tamBloque = N / nproces;



    //    matrizTemp = (double**) malloc(sizeof *matrizTemp * tamBloque);
    //    for (i = 0; i < tamBloque; i++) {
    //        matrizTemp[i] = (double*) malloc(sizeof *matrizTemp[i] * N);
    //    }

    if (myrank == 0) {
        //genración de elementos en el proceso 0
        matrizA = (double**) malloc(sizeof *matrizA * N);
        matrizB = (double**) malloc(sizeof *matrizB * N);
        matrizResultado = (double**) malloc(sizeof *matrizResultado * N);
        vectorTemp = (double*) malloc(sizeof *vectorTemp * N);
        for (i = 0; i < N; i++) {
            matrizA[i] = (double*) malloc(sizeof *matrizA[i] * N);
            matrizB[i] = (double*) malloc(sizeof *matrizB[i] * N);
            matrizResultado[i] = (double*) malloc(sizeof *matrizResultado[i] * N);
        }

        //rellenamos los elementos
        for (i = 0; i < N; i++) {
            for (j = 0; j < N; j++) {
                matrizA[i][j] = (double) drand48();
                matrizB[i][j] = (double) drand48();
                //matrizA[i][j] = (double) i + 1 + j;
                //matrizB[i][j] = (double) i + 1 + j;
                matrizResultado[i][j] = 0;
            }
        }
        if (muestraInfo == 1) {
            printf("\n\n matriz A\n");

            for (i = 0; i < N; i++) {
                for (j = 0; j < N; j++) {
                    printf(" %.2f", matrizA[i][j]);
                }
                printf("\n");
            }

            printf("\n\n matriz B\n");

            for (i = 0; i < N; i++) {
                for (j = 0; j < N; j++) {
                    printf(" %.2f", matrizB[i][j]);
                }
                printf("\n");
            }
        }

    } else {
        //generados de elementos en el resto de procesos
        matrizA = (double**) malloc(sizeof *matrizA * tamBloque);
        matrizResultado = (double**) malloc(sizeof *matrizResultado * tamBloque);
        for (i = 0; i < tamBloque; i++) { //reservamos el numero de filas que corresponde a lo que utilizará cada proceso
            matrizA[i] = (double*) malloc(sizeof *matrizA[i] * N);
            matrizResultado[i] = (double*) malloc(sizeof *matrizResultado[i] * N);
        }
        matrizB = (double**) malloc(sizeof *matrizB * N);
        for (i = 0; i < N; i++) {
            matrizB[i] = (double*) malloc(sizeof *matrizB[i] * N);
        }
    }




    //Envio de elementos
    if (myrank == 0) {
        for (j = 1; j < nproces; j++) {
            for (i = 0; i < tamBloque; i++) {
                MPI_Send(&matrizA[(tamBloque * j) + i][0], N, MPI_DOUBLE, j, i, MPI_COMM_WORLD);
            }
        }
    } else {
        for (i = 0; i < tamBloque; i++) {
            MPI_Recv(matrizA[i], N, MPI_DOUBLE, 0, i, MPI_COMM_WORLD, &status);
        }

    }

    //Envio de elementos
    if (myrank == 0) {
        for (j = 1; j < nproces; j++) {
            for (i = 0; i < N; i++) {
                MPI_Send(&matrizB[i][0], N, MPI_DOUBLE, j, i, MPI_COMM_WORLD);
            }
        }
    } else {
        for (i = 0; i < N; i++) {
            MPI_Recv(matrizB[i], N, MPI_DOUBLE, 0, i, MPI_COMM_WORLD, &status);
        }

    }


    if (myrank == 0) {
        tStart = MPI_Wtime();
    }


    //operaciones
    for (i = 0; i < tamBloque; i++) {
        for (j = 0; j < N; j++) {
            for (k = 0; k < N; k++) {
                matrizResultado[i][j] += matrizA[i][k] * matrizB[k][j];
            }

        }
    }
    //impresion de los componentes parciales
    //    printf("\n\n soy el proceso %d\n", myrank);
    //
    //    for (i = 0; i < tamBloque; i++) {
    //        for (j = 0; j < N; j++) {
    //            printf(" %.2f", matrizResultado[i][j]);
    //        }
    //        printf("\n");
    //    }


    if (myrank == 0) {
        for (i = 1; i < nproces; i++) {
            for (j = 0; j < tamBloque; j++) {
                MPI_Recv(vectorTemp, N, MPI_DOUBLE, MPI_ANY_SOURCE, j, MPI_COMM_WORLD, &status);
                //matrizResultado[(tamBloque * j) + i]=vectorTemp[0];
                for (k = 0; k < N; k++) {
                    matrizResultado[(tamBloque * status.MPI_SOURCE) + j][k] = vectorTemp[k];
                }
            }
        }
    } else {
        for (i = 0; i < tamBloque; i++) {
            MPI_Send(&matrizResultado[i][0], N, MPI_DOUBLE, 0, i, MPI_COMM_WORLD);
        }
    }



    //resultado
    if (myrank == 0) {
        tEnd = MPI_Wtime();
        if (csv == 1) {
            printf("%d;%d;%.41f\n", nproces,N, tEnd - tStart);
        } else {
            printf("Tiempo total en segundos: %.41f para N = %d y %d procesos\n", tEnd - tStart, N, nproces);
        }
    }
    if (muestraInfo == 1) {

        if (myrank == 0) {
            printf("\n\n Matriz Resultado %d\n", myrank);
            for (i = 0; i < N; i++) {
                for (j = 0; j < N; j++) {
                    printf("\t %.2f", matrizResultado[i][j]);
                }
                printf("\n");
            }
        }
    }

    if (myrank == 0) {
        free(matrizA);
        free(vectorTemp);
    }
    free(matrizB);
    free(matrizResultado);

    MPI_Finalize();
}
