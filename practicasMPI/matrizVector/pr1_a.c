#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <locale.h>

//mpicc -o pr1 pr1.c && mpirun -np 4 pr1 -nprocess=5


int N = 12000;
int muestraInfo = 0;
int csv = 0;
//Programa valido para N divisible entero por np

int main(int argc, char *argv[]) {
    setlocale(LC_ALL,"es_ES.UTF-8");

    int nproces, myrank, i, j;
    double **matriz;
    double *vector, *vectorResultado, *temp;
    int tamBloque;
    double tStart, tEnd;

    srand(time(NULL));
    srand48(time(NULL));

    MPI_Status status;

    MPI_Init(&argc, &argv);

    int nAux = atoi(argv[1]);
    if (nAux) {
        N = nAux;
    }
    if (muestraInfo == 1) {
        printf("Soy el proceso %d... con N = %d", myrank, N);
    }

    //~ char csvAux = ;
    //~ char csvAux = ;
    if (argv[2] && (atoi(argv[2]) == 1)) {
        csv = 1;
    }

    MPI_Comm_size(MPI_COMM_WORLD, &nproces);
    MPI_Comm_rank(MPI_COMM_WORLD, &myrank);

    tamBloque = N / nproces;

    temp = (double*) malloc(sizeof *temp * tamBloque);

    if (myrank == 0) {
        //genraci�n de elementos en el proceso 0
        matriz = (double**) malloc(sizeof *matriz * N);
        for (i = 0; i < N; i++) {
            matriz[i] = (double*) malloc(sizeof *matriz[i] * N);
        }
        vector = (double*) malloc(sizeof *vector * N);
        vectorResultado = (double*) malloc(sizeof *vectorResultado * N);

        //rellenamos los elementos
        for (i = 0; i < N; i++) {
            for (j = 0; j < N; j++) {
                matriz[i][j] = (double) drand48();
            }
        }

        for (i = 0; i < N; i++) {
            vector[i] = (double) drand48();
        }
        for (i = 0; i < N; i++) {
            vectorResultado[i] = 0;
        }

    } else {
        //genraci�n de elementos en el resto de procesos
        matriz = (double**) malloc(sizeof *matriz * tamBloque);
        for (i = 0; i < N; i++) {
            matriz[i] = (double*) malloc(sizeof *matriz[i] * N);
        }
        vector = (double*) malloc(sizeof *vector * N);
        vectorResultado = (double*) malloc(sizeof *vectorResultado * tamBloque);
        for (i = 0; i < tamBloque; i++) {
            vectorResultado[i] = 0;
        }
    }

    //Envio de elementos
    if (myrank == 0) {
        for (j = 1; j < nproces; j++) {
            for (i = 0; i < tamBloque; i++) {
                MPI_Send(&matriz[tamBloque * j + i][0], N, MPI_DOUBLE, j, i, MPI_COMM_WORLD);
            }
        }
        for (i = 1; i < nproces; i++) {
            MPI_Send(&vector[0], N, MPI_DOUBLE, i, 5, MPI_COMM_WORLD);
        }
    } else {
        for (i = 0; i < tamBloque; i++) {
            MPI_Recv(matriz[i], N, MPI_DOUBLE, 0, i, MPI_COMM_WORLD, &status);
        }
        MPI_Recv(vector, N, MPI_DOUBLE, 0, 5, MPI_COMM_WORLD, &status);
    }
    if (myrank == 0) {

        tStart = MPI_Wtime();
    }

    //operaciones
    for (i = 0; i < tamBloque; i++) {
        for (j = 0; j < N; j++) {
            vectorResultado[i] += matriz[i][j] * vector[j];
        }
    }

    //recepcion de los datos
    if (myrank == 0) {
        for (i = 1; i < nproces; i++) {
            //forzamos el orden  de recepcion estableciendo i como el proceso que queremos escuchar
            MPI_Recv(temp, tamBloque, MPI_DOUBLE, i, 5, MPI_COMM_WORLD, &status);
            for (j = 1; j < nproces; j++) {
                vectorResultado[tamBloque * i + j] = temp[j];
            }
        }
    } else {
        MPI_Send(&vectorResultado[0], tamBloque, MPI_DOUBLE, 0, 5, MPI_COMM_WORLD);
    }

    //resultado
    if (myrank == 0) {
        tEnd = MPI_Wtime();

        if (csv == 1) {
            printf("%d;%d;%.41f\n", nproces,N, tEnd - tStart);
        } else {
            printf("Tiempo total en segundos: %.41f para N = %d y %d procesos\n", tEnd - tStart, N, nproces);
        }
    }

    if (myrank == 0) {
        free(matriz);
        free(temp);
    }
    free(vector);
    free(vectorResultado);

    MPI_Finalize();
}

