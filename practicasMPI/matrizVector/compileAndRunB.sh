# Archivo de generación de la practica
# parametro 1: N
# parametro 2: csv = 1
#
mpicc -o pr1 pr1_a.c
if [ -f ./pr1 ]; then
echo 'numero procesos;N;tiempo'
mpirun -np 1 pr1 2000 1
mpirun -np 1 pr1 4000 1
mpirun -np 1 pr1 6000 1
mpirun -np 1 pr1 8000 1
mpirun -np 1 pr1 10000 1
mpirun -np 1 pr1 12000 1
mpirun -np 1 pr1 14000 1
#mpirun -np 4 pr1 16000 1
#mpirun -np 4 pr1 18000 1
#mpirun -np 4 pr1 20000 1
mpirun -np 4 pr1 2000 1
mpirun -np 4 pr1 4000 1
mpirun -np 4 pr1 6000 1
mpirun -np 4 pr1 8000 1
mpirun -np 4 pr1 10000 1
mpirun -np 4 pr1 12000 1
mpirun -np 4 pr1 14000 1
#mpirun -np 4 pr1 16000 1
#mpirun -np 4 pr1 18000 1
#mpirun -np 4 pr1 20000 1
rm pr1
rm pr1_a.o
fi

