#matrizVector mpi
if [ -f ./practicasMPI/matrizVector/compileAndRunA.sh ]; then
    echo 'De 1 a 4 procesos con N = 12000:   ./practicasMPI/matrizVector/compileAndRunA.sh'
    cd practicasMPI/matrizVector
    ./compileAndRunA.sh > ../../resultadoMpiMatizVectorA.csv
    cd ../../
fi
if [ -f ./practicasMPI/matrizVector/compileAndRunB.sh ]; then
    echo 'De 1 y 4 procesos con N variable:  ./practicasMPI/matrizVector/compileAndRunB.sh'
    cd practicasMPI/matrizVector
    ./compileAndRunB.sh > ../../resultadoMpiMatizVectorB.csv
    cd ../../
fi
#matrizMatriz mpi
if [ -f ./practicasMPI/matrizMatriz/compileAndRunA.sh ]; then
    echo 'De 1 a 4 procesos con N = 1200:    ./practicasMPI/matrizMatriz/compileAndRunA.sh'
    cd practicasMPI/matrizMatriz
    ./compileAndRunA.sh > ../../resultadoMpiMatizMatrizA.csv
    cd ../../
fi
if [ -f ./practicasMPI/matrizMatriz/compileAndRunB.sh ]; then
    echo 'De 1 y 4 procesos con N variable:  ./practicasMPI/matrizMatriz/compileAndRunB.sh'
    cd practicasMPI/matrizMatriz
    ./compileAndRunB.sh > ../../resultadoMpiMatizMatrizB.csv
    cd ../../
fi
#matrizVector omp
if [ -f ./practicasOpenMP/matrizVector/compileAndRun.sh ]; then
    echo 'De 1 a 4 procesos con N = 12000:   ./practicasOpenMP/matrizVector/compileAndRun.sh'
    cd practicasOpenMP/matrizVector
    ./compileAndRun.sh > ../../resultadoOmpMatizVector.csv
    cd ../../
fi
#matrizMatriz omp
if [ -f ./practicasOpenMP/matrizMatriz/compileAndRun.sh ]; then
    echo 'De 1 a 4 procesos con N = 1200:    ./practicasOpenMP/matrizMatriz/compileAndRun.sh'
    cd practicasOpenMP/matrizMatriz
    ./compileAndRun.sh > ../../resultadoOmpMatizMatriz.csv
    cd ../../
fi


